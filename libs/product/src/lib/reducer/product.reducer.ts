import { Action } from '@ngrx/store';
import { Product } from '@angular-demo/product';
import * as ProductActions from '../action/product.action';

export const initalState: Product[] = [];

export function reducer(state = initalState, action: ProductActions.Actions) {
  switch(action.type) {
    case ProductActions.GET_PRODUCT:
      return action.payload;
    case ProductActions.ADD_PRODUCT:
      return [...state, action.payload];
    case ProductActions.EDIT_PRODUCT:
      return state.map(item => {
        if (item.id === action.payload.id) {
          item = action.payload;
        }
        return item;
      });
    case ProductActions.REMOVE_PRODUCT:
      return state.filter(item => {
        return action.payload.id !== item.id;
      });
    default:
      return state;
  }
}

