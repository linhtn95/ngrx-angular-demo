import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Product } from '../model/product';

export const GET_PRODUCT = '[PRODUCT] Get';
export const ADD_PRODUCT = '[PRODUCT] Add';
export const EDIT_PRODUCT = '[PRODUCT] Edit';
export const REMOVE_PRODUCT = '[PRODUCT] Remove';

export class GetProduct implements Action {
  readonly type = GET_PRODUCT;

  constructor(public payload: any = null) { }
}

export class AddProduct implements Action {
  readonly type = ADD_PRODUCT;

  constructor(public payload: any = null) { }
}

export class EditProduct implements Action {
  readonly type = EDIT_PRODUCT;

  constructor(public payload: any = null) { }
}

export class RemoveProduct implements Action {
  readonly type = REMOVE_PRODUCT;

  constructor(public payload: any = null) { }
}

export type Actions =  GetProduct | AddProduct | EditProduct | RemoveProduct;
