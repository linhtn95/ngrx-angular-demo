import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { LayoutComponent } from './components/layout/layout.component';
import { FormDataComponent } from './components/form-data/form-data.component';
import { ListDataComponent } from './components/list-data/list-data.component';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';
import { FormsModule } from '@angular/forms';
import { ProductService } from './services/product.service';
import { StoreModule } from '@ngrx/store';
import { reducer } from './reducer/product.reducer';


@NgModule({
  imports: [
    CommonModule,
    StoreModule.forRoot({
      product: reducer
    }),
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    InputTextModule,
    ButtonModule,
    TableModule,
    DialogModule,
    RouterModule.forChild([
      {
        path: '',
        component: LayoutComponent,
        children: [
          {
            path: '',
            component: FormDataComponent
          }
        ]
      }
    ])
  ],
  declarations: [LayoutComponent, FormDataComponent, ListDataComponent],
  providers: [ProductService]
})
export class ProductModule {}
