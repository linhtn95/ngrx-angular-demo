import { Component, OnInit } from '@angular/core';
import { Product } from '../../model/product';
import { ProductService } from '../../services/product.service';
import { Store, select } from '@ngrx/store';
import { AppState } from '../../app.state';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import * as ProductActions from '../../action/product.action';

@Component({
  selector: 'angular-demo-form-data',
  templateUrl: './form-data.component.html',
  styleUrls: ['./form-data.component.css']
})
export class FormDataComponent implements OnInit {

  product: Product = new Product();
  products: Observable<Product[]>;
  display: boolean = false;

  constructor(private store: Store<AppState>, private productService: ProductService ) {
    this.products = store.pipe(select('product'));
  }

  ngOnInit() {
    this.productService.getAllProducts().subscribe(
      value => {
        this.store.dispatch({ type: ProductActions.GET_PRODUCT, payload: value });
        console.log('value', value);
      },
      error => {
        console.log(error);
      }
    );
  }

  add() {
    this.productService.addProduct(this.product)
    .pipe(
      catchError(() => of(new Error()))
    )
    .subscribe(
      product => {
        this.store.dispatch(new ProductActions.AddProduct(product));
      },
      error => {
        console.log(error);
      }
    )
  }

  remove(product) {
    this.productService.removeProduct(product)
    .pipe(
      catchError(() => of(new Error()))
    )
    .subscribe(
      product => {
        this.store.dispatch(new ProductActions.RemoveProduct(product));
      },
      error => {
        console.log(error);
      }
    )
  }

  showDialog(product) {
    this.product = product;
    this.display = true;
  }

  edit(product) {
    console.log('product', product);
    this.productService.editProduct(product)
    .pipe(
      catchError(() => of(new Error()))
    )
    .subscribe(
      product => {
        this.store.dispatch(new ProductActions.EditProduct(product));
      },
      error => {
        console.log(error);
      }
    )
    this.display = false;
  }

}
