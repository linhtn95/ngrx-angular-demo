import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product } from '../model/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  public mockAPI = `http://5b9dd9f5133f660014c918f1.mockapi.io/product`;

  constructor(private httpClient: HttpClient) { }

  getAllProducts(): Observable<Product[]> {
    return this.httpClient.get<Product[]>(this.mockAPI);
  }

  addProduct(product: Product): Observable<Product> {
    return this.httpClient.post<Product>(this.mockAPI, product);
  }

  editProduct(product: Product): Observable<Product> {
    return this.httpClient.put<Product>(this.mockAPI + `/${product.id}`, product);
  }

  removeProduct(product: Product): Observable<Product> {
    return this.httpClient.delete<Product>(this.mockAPI + `/${product.id}`);
  }
}
