import { Routes, RouterModule } from "@angular/router";


export const appRoutes: Routes = [
    {
        path: '', redirectTo: 'home', pathMatch: 'full'
    },
    {
        path: 'home', loadChildren: '@angular-demo/product#ProductModule'
    }
];

export const AppRoutingModule = RouterModule.forRoot(appRoutes, {useHash: true});
